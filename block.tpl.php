<?php /* $Id$ */ ?>
<div id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>" class="block<?php print " block-$block->module"; ?><?php print ($block->region == 1 || $block->region == 'right') ? ' rightblock': ' leftblock'; ?>">

<?php if ($block->region == 'left') { ?><h3 ><?php print $block->subject; ?></h3>
<?php } else { ?><h3><?php print $block->subject; ?></h3><?php } ?>
		
<div class="content"><?php print $block->content; ?></div>
</div>
