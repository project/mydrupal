MyDrupal01 theme is derived from Bluemarine and it has lot of features added for a complete community drupal site. 

1: Header image provided in 3 colors, just by replacing headerbg.gif with headerbg_darkred.gif, headerbg_olive.gif or headerbg_gray.gif you can totally change the look of your headers.

2. All images are in images folder and you can customize your look by changing the images.

3. Drupal does not have icons and images, which makes the website very dry. MyDrupal01 has icons for all your links. This gives all nodes an enhanced look

4. Forums looks has been enhanced too.

This has been tested in IE7 and Firefox.


PLEASE DO NOT REMOVE the footer information "Theme designed by My Drupal". 
For Questions/Comments please visit forums on mydrupal.com 

Thanks.

