<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
 <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body >

<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td id="logo" colspan="1">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><div class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></div><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </td>
		  <td align="right" valign="top" >
	  <?php if (isset($secondary_links)) { ?><div id="secondary">
	  <?php print theme('links', $secondary_links) ?></div>
	  <?php } ?>
	  </td>	 
  </tr>
  <tr>
	      <td id="menu"  >
		<?php if (count($primary_links)) : ?>
		<ul id="primary">
		<span>
			<?php foreach ($primary_links as $link): ?> 
				<?php $class = ""; ?> 

				<?php if ( stristr($link, 'active')  ) :  ?> 
					<?php $class = 'id="current"'; ?> 
				<?php endif; ?>

				<li <?php print $class?>><?php print $link?></li>  

			<?php endforeach; ?></span>
		</ul>
	  <?php endif; ?>
	</td> 
<td align="right" height="15px" >
		<?php print $search_box ?>
	</td>

	</tr>
<tr>
<td colspan="1"><div><?php print $header ?></div></td>
</tr>

</table>
<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>

<h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<div id="footer">
  <?php print $footer_message ?>
 <br> Theme designed by: <a href="http://mydrupal.com">My Drupal</a>
</div>
<?php print $closure ?>

</body>
</html>
